# Convert MLF2 S-expression data files to netCDF

This repository contains a new Go program for converting MLF2 "S-exp" data files to netCDF. This replaces the old Python script. It runs much faster and can be distributed as a single executable for Linux, MacOS, and Windows systems.

## Installation

Download the latest release from the *Downloads* section of this repository and unpack on the target system.

## Usage

``` shellsession
$ sexpcdf --help
sexpcdf [options] infile

Convert an MLF2 S-expression data file to a set of netCDF files.

  -a value
    	add a global attribute to the netCDF files NAME=VAL
  -version
    	Show program version information and exit
  -z	create a ZIP archive of netCDF files
```

If the *-z* option is specified, the netCDF files will be packaged as a ZIP archive, otherwise they will be stored in a directory named after the input S-exp filename (converted to lowercase).

``` shellsession
$ sexpcdf -z -a floatid=88 ~/Downloads/data0003.sx
2021/08/04 12:09:30 Creating data0003/flntu_0003.nc ...
2021/08/04 12:09:30 Creating data0003/sbe63_0003.nc ...
$ ls *.zip
data0003.zip
$ unzip -t data0003.zip
Archive:  data0003.zip
    testing: flntu_0003.nc            OK
    testing: sbe63_0003.nc            OK
No errors detected in compressed data of data0003.zip.
```

Note that SUNA files are stored in the Satlantic binary format rather than netCDF.
