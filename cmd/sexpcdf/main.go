// Sexpcdf converts MLF2 S-expression data files to netCDF.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"

	"bitbucket.org/uwaplmlf2/sexpcdf/internal/app"
)

var Version = "dev"
var BuildDate = "unknown"

const Usage = `sexpcdf [options] infile

Convert an MLF2 S-expression data file to a set of netCDF files.

`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	mkArchive   bool
	globalAttrs app.Pairs
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&mkArchive, "z", mkArchive, "create a ZIP archive of netCDF files")
	flag.Var(&globalAttrs, "a", "add a global attribute to the netCDF files NAME=VAL")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var err error

	f, err := os.Open(args[0])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	id, err := app.NewStoreId(filepath.Base(args[0]))
	if err != nil {
		log.Fatalf("Invalid filename format: %q", args[0])
	}

	if mkArchive {
		file, err := os.Create(fmt.Sprintf("%s.zip", id.String()))
		if err != nil {
			log.Fatalf("create ZIP file: %v", err)
		}
		defer file.Close()
		err = app.Run(f, id, globalAttrs, file)
	} else {
		err = app.Run(f, id, globalAttrs, nil)
	}

	if err != nil {
		log.Println(err)
	}
}
