package main

import (
	"fmt"
	"log"
	"time"

	"github.com/streadway/amqp"
)

type MsgHandler func(contents []byte, hdr amqp.Table) ([]byte, error)

func mqSetup(uri, exch, qname string) (*amqp.Connection, *amqp.Channel, error) {
	conn, err := amqp.Dial(uri)
	if err != nil {
		return nil, nil, fmt.Errorf("connect: %w", err)
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, nil, fmt.Errorf("open channel: %w", err)
	}

	if err = channel.ExchangeDeclare(
		exch,    // name of the exchange
		"topic", // type
		true,    // durable
		false,   // delete when complete
		false,   // internal
		false,   // noWait
		nil,     // arguments
	); err != nil {
		return nil, nil, fmt.Errorf("exchange declare: %w", err)
	}

	if _, err = channel.QueueDeclare(
		qname, // name of the queue
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	); err != nil {
		return nil, nil, fmt.Errorf("queue declare: %w", err)
	}

	if err = channel.QueueBind(
		qname, // name of the queue
		qname, // bindingKey
		exch,  // sourceExchange
		false, // noWait
		nil,   // arguments
	); err != nil {
		return nil, nil, fmt.Errorf("queue bind: %w", err)
	}

	return conn, channel, nil
}

func mqConsume(uri, exch, qname string) (*amqp.Channel, <-chan amqp.Delivery, error) {
	_, ch, err := mqSetup(uri, exch, qname)
	if err != nil {
		return nil, nil, err
	}

	// Indicate we only want 1 message to acknowledge at a time.
	if err := ch.Qos(1, 0, false); err != nil {
		return nil, nil, err
	}

	deliveries, err := ch.Consume(
		qname, // name
		qname, // consumerTag,
		false, // noAck
		false, // exclusive
		false, // noLocal
		false, // noWait
		nil,   // arguments
	)

	return ch, deliveries, err
}

// rdMsgs is the AMQP message consumer
func rdMsgs(channel *amqp.Channel, ch <-chan amqp.Delivery, handler MsgHandler) {
	for msg := range ch {
		filename, ok := msg.Headers["filename"]
		if !ok {
			log.Printf("No filename in message header")
			continue
		}
		if _, ok = filename.(string); !ok {
			log.Printf("Invalid type for 'filename': %T", filename)
			continue
		}

		data, err := handler(msg.Body, msg.Headers)
		if err != nil {
			log.Printf("Cannot process %v: %v", filename, err)
			msg.Reject(false)
			continue
		}

		reply := amqp.Publishing{
			Headers:      msg.Headers,
			ContentType:  "application/zip",
			DeliveryMode: 2,
			Timestamp:    time.Now(),
			Body:         data,
		}

		if err := channel.Publish(amqpExch, msg.ReplyTo,
			false, false, reply); err != nil {
			log.Printf("Publish failed: %v", err)
		}

		msg.Ack(false)

		log.Printf("Converted %v to %v", filename, msg.Headers["filename"])
	}

	log.Println("Message channel closed")
}
