// Sexprpc provides a service which monitors a RabbitMQ queue for MLF2
// S-exp data files and converts them to a series of netCDF files packaged
// in a ZIP archive.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"bitbucket.org/uwaplmlf2/sexpcdf/internal/app"
	"github.com/streadway/amqp"
)

const Usage = `Usage: sexprpc [options]

Listen on a RabbitMQ queue for MLF2 s-exp data files and convert them to netCDF.

`

var Version = "dev"
var BuildDate = "unknown"

const (
	amqpVhost = "mlf2"
	amqpQname = "sexp_convert"
)

var (
	showVers        = flag.Bool("version", false, "Show program version information and exit")
	amqpAddr string = "50.18.188.61:5672"
	amqpUser string = "guest:guest"
	amqpExch string = "data"
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&amqpAddr, "addr", lookupEnvOrString("AMQP_ADDR", amqpAddr),
		"Set host:port for AMQP message broker")
	flag.StringVar(&amqpUser, "user", lookupEnvOrString("AMQP_USER", amqpUser),
		"Set user:password for AMQP message broker")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func convertFile(contents []byte, hdr amqp.Table) ([]byte, error) {
	dir, err := os.MkdirTemp("", "sexpcdf")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(dir)

	// Get our current directory so we can move back to
	// it on return
	cwd, _ := os.Getwd()
	defer os.Chdir(cwd)
	// Move into the temp directory
	os.Chdir(dir)

	id, err := app.NewStoreId(hdr["filename"].(string))
	if err != nil {
		return nil, err
	}

	pairs := make([]app.Pair, 0)
	if floatid, ok := hdr["floatid"]; ok {
		pairs = append(pairs,
			app.Pair{Name: "floatid", Value: fmt.Sprintf("%v", floatid)})
	}

	var output bytes.Buffer
	app.Run(bytes.NewReader(contents), id, pairs, &output)

	// Update the "filename" entry in the header
	hdr["filename"] = id.String() + ".zip"

	return output.Bytes(), nil
}

func main() {
	_ = parseCmdLine()

	u := url.URL{}
	u.Scheme = "amqp"
	u.Host = amqpAddr
	u.Path = "mlf2"

	creds := strings.Split(amqpUser, ":")
	if len(creds) != 2 {
		log.Fatalf("Invalid AMQP credentials: %q", amqpUser)
	}
	u.User = url.UserPassword(creds[0], creds[1])

	_, pub, err := mqSetup(u.String(), amqpExch, amqpQname)
	if err != nil {
		log.Fatalf("Cannot access AMQP broker: %v", err)
	}

	channel, deliveries, err := mqConsume(u.String(), amqpExch, amqpQname)
	if err != nil {
		log.Fatalf("Cannot access AMQP broker: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			// Cancel the message consumer
			channel.Cancel(amqpQname, false)
		}
	}()

	log.Printf("Starting s-exp file converter %s", Version)

	// Start message consumer
	rdMsgs(pub, deliveries, convertFile)

}
