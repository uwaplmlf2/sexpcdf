module bitbucket.org/uwaplmlf2/sexpcdf

go 1.13

require (
	bitbucket.org/ctessum/cdf v0.0.0-20161214184548-ca25c8d1d911
	bitbucket.org/uwaplmlf2/mlf2sexp v0.7.0
	github.com/streadway/amqp v1.0.0
)
