package ncfile

import (
	"errors"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"time"

	"bitbucket.org/ctessum/cdf"
)

// Var represents a field of a struct
type Var struct {
	Name  string
	Dtype reflect.Type
	Index int
	Dims  []string
	Attrs map[string]string
}

func (v Var) MakeValue() interface{} {
	return reflect.MakeSlice(reflect.SliceOf(v.Dtype), 0, 0).Interface()
}

// Vars takes a struct as input and returns the type and a slice of Vars.
func Vars(T interface{}) (reflect.Type, []Var) {
	vars := make([]Var, 0)
	t := reflect.TypeOf(T)
	if t.Kind() != reflect.Struct {
		return t, vars
	}
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		dv := Var{Dtype: f.Type, Index: i, Attrs: make(map[string]string)}
		if v, ok := f.Tag.Lookup("var"); ok {
			dv.Name = v
		} else {
			dv.Name = f.Name
		}

		if v, ok := f.Tag.Lookup("attrs"); ok {
			for _, attr := range strings.Split(v, ",") {
				parts := strings.Split(attr, "=")
				if len(parts) == 2 {
					dv.Attrs[parts[0]] = parts[1]
				} else {
					log.Printf("Warning; Invalid 'attrs' tag format: %q", attr)
				}
			}
		}

		if v, ok := f.Tag.Lookup("dims"); ok {
			if f.Type.Kind() != reflect.Array {
				log.Printf("Warning; dims tag requires array variable")
				dv.Dims = []string{}
			} else {
				dv.Dims = strings.Split(v, ",")
			}
		} else {
			dv.Dims = []string{}
		}

		vars = append(vars, dv)
	}

	return t, vars
}

type NetcdfFile struct {
	ff    *os.File
	f     *cdf.File
	hdr   *cdf.Header
	dtype reflect.Type
	dvars []Var
}

type Attributes map[string]map[string]interface{}

// Create an MLF2 netCDF data file using the fields of the struct v. Each field of the struct
// will become a separate record variable in the file.
func NewFile(path string, v interface{}, attrs Attributes) (*NetcdfFile, error) {
	dtype, dvars := Vars(v)
	if len(dvars) == 0 {
		return nil, errors.New("No variables found")
	}

	return newFile(path, dtype, dvars, attrs)
}

func newFile(path string, dtype reflect.Type, dvars []Var, attrs Attributes) (*NetcdfFile, error) {
	var err error

	nf := &NetcdfFile{dvars: dvars, dtype: dtype}
	nf.ff, err = os.Create(path)
	if err != nil {
		return nil, fmt.Errorf("Cannot open %q: %w", path, err)
	}

	// Determine the lengths of the non-record dimensions.
	names := []string{"time"}
	lengths := []int{0}
	dims := make(map[string]bool)
	for _, v := range dvars {
		for _, name := range v.Dims {
			if !dims[name] {
				dims[name] = true
				names = append(names, name)
				lengths = append(lengths, v.Dtype.Len())
			}
		}
	}

	nf.hdr = cdf.NewHeader(names, lengths)
	for _, v := range dvars {
		if v.Name == "time" {
			nf.hdr.AddVariable(v.Name, []string{"time"}, []int32{})
		} else {
			nf.hdr.AddVariable(v.Name,
				append([]string{"time"}, v.Dims...), v.MakeValue())
		}
		if a, ok := attrs[v.Name]; ok {
			for key, val := range a {
				nf.hdr.AddAttribute(v.Name, key, val)
			}
		}

		for key, val := range v.Attrs {
			nf.hdr.AddAttribute(v.Name, key, val)
		}
	}

	// Add global attributes
	if a, ok := attrs[""]; ok {
		for key, val := range a {
			nf.hdr.AddAttribute("", key, val)
		}
	}
	// Make the header immutable
	nf.hdr.Define()
	nf.f, err = cdf.Create(nf.ff, nf.hdr)

	return nf, nil
}

func (nf *NetcdfFile) Close() error {
	cdf.UpdateNumRecs(nf.ff)
	return nf.ff.Close()
}

func fillTimeSlice(src reflect.Value, idx int, dst []int32) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		t := v.Field(idx).Interface().(time.Time)
		dst[i] = int32(t.Unix())
	}
}

func fillUint8Slice(src reflect.Value, idx int, dst []uint8) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = uint8(v.Field(idx).Uint())
	}
}

func fillUint8ArraySlice(src reflect.Value, idx int, dst []uint8) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = uint8(v.Index(j).Uint())
			k++
		}
	}
}

func fillInt8Slice(src reflect.Value, idx int, dst []int8) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = int8(v.Field(idx).Int())
	}
}

func fillInt8ArraySlice(src reflect.Value, idx int, dst []int8) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = int8(v.Index(j).Int())
			k++
		}
	}
}

func fillInt16Slice(src reflect.Value, idx int, dst []int16) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = int16(v.Field(idx).Int())
	}
}

func fillInt16ArraySlice(src reflect.Value, idx int, dst []int16) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = int16(v.Index(j).Int())
			k++
		}
	}
}

func fillInt32Slice(src reflect.Value, idx int, dst []int32) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = int32(v.Field(idx).Int())
	}
}

func fillInt32ArraySlice(src reflect.Value, idx int, dst []int32) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = int32(v.Index(j).Int())
			k++
		}
	}
}

func fillFloat32Slice(src reflect.Value, idx int, dst []float32) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = float32(v.Field(idx).Float())
	}
}

func fillFloat32ArraySlice(src reflect.Value, idx int, dst []float32) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = float32(v.Index(j).Float())
			k++
		}
	}
}

func fillFloat64Slice(src reflect.Value, idx int, dst []float64) {
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i)
		dst[i] = v.Field(idx).Float()
	}
}

func fillFloat64ArraySlice(src reflect.Value, idx int, dst []float64) {
	k := int(0)
	for i := 0; i < src.Len(); i++ {
		v := src.Index(i).Field(idx)
		for j := 0; j < v.Len(); j++ {
			dst[k] = v.Index(j).Float()
			k++
		}
	}
}

func extractField(val reflect.Value, dvar Var) (interface{}, error) {
	var dst interface{}
	switch dvar.Dtype.Kind() {
	case reflect.Struct:
		switch dvar.Dtype.Name() {
		case "Time":
			dst = make([]int32, val.Len())
			fillTimeSlice(val, dvar.Index, dst.([]int32))
		default:
			return nil, fmt.Errorf("Unsupported struct: %q", dvar.Dtype.Name())
		}
	case reflect.Uint8:
		dst = make([]uint8, val.Len())
		fillUint8Slice(val, dvar.Index, dst.([]uint8))
	case reflect.Int8:
		dst = make([]int8, val.Len())
		fillInt8Slice(val, dvar.Index, dst.([]int8))
	case reflect.Int16:
		dst = make([]int16, val.Len())
		fillInt16Slice(val, dvar.Index, dst.([]int16))
	case reflect.Int32:
		dst = make([]int32, val.Len())
		fillInt32Slice(val, dvar.Index, dst.([]int32))
	case reflect.Float32:
		dst = make([]float32, val.Len())
		fillFloat32Slice(val, dvar.Index, dst.([]float32))
	case reflect.Float64:
		dst = make([]float64, val.Len())
		fillFloat64Slice(val, dvar.Index, dst.([]float64))
	case reflect.Array:
		n := val.Len() * dvar.Dtype.Len()
		switch dvar.Dtype.Elem().Kind() {
		case reflect.Uint8:
			dst = make([]uint8, n)
			fillUint8ArraySlice(val, dvar.Index, dst.([]uint8))
		case reflect.Int8:
			dst = make([]int8, n)
			fillInt8ArraySlice(val, dvar.Index, dst.([]int8))
		case reflect.Int16:
			dst = make([]int16, n)
			fillInt16ArraySlice(val, dvar.Index, dst.([]int16))
		case reflect.Int32:
			dst = make([]int32, n)
			fillInt32ArraySlice(val, dvar.Index, dst.([]int32))
		case reflect.Float32:
			dst = make([]float32, n)
			fillFloat32ArraySlice(val, dvar.Index, dst.([]float32))
		case reflect.Float64:
			dst = make([]float64, n)
			fillFloat64ArraySlice(val, dvar.Index, dst.([]float64))
		default:
			return nil, fmt.Errorf("Unsupported element type: %s", dvar.Dtype.Elem().Kind())
		}
	default:
		return nil, fmt.Errorf("Unsupported element type: %s", dvar.Dtype.Kind())
	}

	return dst, nil
}

// WriteRecords appends data records to a netCDF file. v must be a slice of structs
// containing the values for each record.
func (nf *NetcdfFile) WriteRecords(v interface{}) error {
	val := reflect.ValueOf(v)
	if val.Kind() != reflect.Slice {
		return errors.New("Argument must be a slice")
	}

	// We need to copy each field from the struct into it's own slice
	var (
		dst interface{}
		err error
	)

	for _, dvar := range nf.dvars {
		dst, err = extractField(val, dvar)
		if err != nil {
			return err
		}
		wtr := nf.f.Writer(dvar.Name, nil, nil)
		_, err := wtr.Write(dst)
		if err != nil {
			return fmt.Errorf("Error writing %q: %w", dvar.Name, err)
		}
	}

	cdf.UpdateNumRecs(nf.ff)

	return nil
}

// WriteVar writes a non-record variable to the netCDF file. Non-record
// variables must be written before record variables.
func (nf *NetcdfFile) WriteVar(name string, v interface{}) error {
	val := reflect.ValueOf(v)
	switch val.Kind() {
	case reflect.Array, reflect.Slice:
	default:
		return errors.New("Argument must be an array or slice")
	}

	wtr := nf.f.Writer(name, []int{0}, []int{val.Len() - 1})
	_, err := wtr.Write(v)
	return err
}
