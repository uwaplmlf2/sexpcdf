package ncfile

import (
	"log"
	"reflect"
	"testing"
	"time"
)

func TestVars(t *testing.T) {
	T := struct {
		Ival int16
		Fval float64
	}{}

	expected := []Var{
		{Name: "Ival", Dtype: reflect.TypeOf(int16(0)), Index: 0,
			Dims: []string{}, Attrs: map[string]string{}},
		{Name: "Fval", Dtype: reflect.TypeOf(float64(0)), Index: 1,
			Dims: []string{}, Attrs: map[string]string{}},
	}

	_, vlist := Vars(T)
	for i, v := range vlist {
		if !reflect.DeepEqual(v, expected[i]) {
			t.Errorf("value mismatch; expected %#v, got %#v",
				expected[i], v)
		}
	}

}

func TestTaggedVars(t *testing.T) {
	T := struct {
		Ival int16   `var:"foo"`
		Fval float64 `var:"bar"`
	}{}

	expected := []Var{
		{Name: "foo", Dtype: reflect.TypeOf(int16(0)), Index: 0,
			Dims: []string{}, Attrs: map[string]string{}},
		{Name: "bar", Dtype: reflect.TypeOf(float64(0)), Index: 1,
			Dims: []string{}, Attrs: map[string]string{}},
	}

	_, vlist := Vars(T)
	for i, v := range vlist {
		if !reflect.DeepEqual(v, expected[i]) {
			t.Errorf("value mismatch; expected %#v, got %#v",
				expected[i], v)
		}
	}

}

func TestAttrsTag(t *testing.T) {
	T := struct {
		Ival int16   `var:"foo" attrs:"a=1,b=2"`
		Fval float64 `var:"bar" attrs:"a=3,b=4"`
		Lval int32   `var:"baz" attrs:"a=5,b"`
	}{}

	expected := []Var{
		{Name: "foo", Dtype: reflect.TypeOf(int16(0)), Index: 0,
			Dims:  []string{},
			Attrs: map[string]string{"a": "1", "b": "2"}},
		{Name: "bar", Dtype: reflect.TypeOf(float64(0)), Index: 1,
			Dims:  []string{},
			Attrs: map[string]string{"a": "3", "b": "4"}},
		{Name: "baz", Dtype: reflect.TypeOf(int32(0)), Index: 2,
			Dims:  []string{},
			Attrs: map[string]string{"a": "5"}},
	}

	_, vlist := Vars(T)
	for i, v := range vlist {
		if !reflect.DeepEqual(v, expected[i]) {
			t.Errorf("value mismatch; expected %#v, got %#v",
				expected[i], v)
		}
	}

}

func TestTypeName(t *testing.T) {
	val := struct {
		T    time.Time
		Fval float64
	}{}

	_, vlist := Vars(val)
	dvar := vlist[0]

	if dvar.Dtype.Kind() != reflect.Struct {
		t.Errorf("Unexpected Kind value: %v", dvar.Dtype.Kind())
	}

	if dvar.Dtype.Name() != "Time" {
		t.Errorf("Unexpected Name value: %v", dvar.Dtype.Name())
	}
}

func TestFillSlice(t *testing.T) {
	vals := []struct {
		Fval float32
		Ival int32
		Aval [2]float32
		T    time.Time
	}{
		{Fval: 5.0, Ival: 5, T: time.Unix(0, 0)},
		{Fval: 4.0, Ival: 4, T: time.Unix(10, 0)},
		{Fval: 3.0, Ival: 3, T: time.Unix(20, 0)},
		{Fval: 2.0, Ival: 2, T: time.Unix(30, 0)},
		{Fval: 1.0, Ival: 1, T: time.Unix(40, 0)},
		{Fval: 0.0, Ival: 0, T: time.Unix(50, 0)},
	}

	_, vlist := Vars(vals[0])

	var x interface{}
	x = vals
	xx := reflect.ValueOf(x)

	buf, err := extractField(xx, vlist[0])
	if err != nil {
		log.Fatal(err)
	}

	fval, ok := buf.([]float32)
	if !ok {
		log.Fatalf("Invalid type: %T", buf)
	}

	buf, err = extractField(xx, vlist[1])
	if err != nil {
		log.Fatal(err)
	}

	ival, ok := buf.([]int32)
	if !ok {
		log.Fatalf("Invalid type: %T", buf)
	}

	buf, err = extractField(xx, vlist[2])
	if err != nil {
		log.Fatal(err)
	}

	aval, ok := buf.([]float32)
	if !ok {
		log.Fatalf("Invalid type: %T", buf)
	}

	buf, err = extractField(xx, vlist[3])
	if err != nil {
		log.Fatal(err)
	}

	tval, ok := buf.([]int32)
	if !ok {
		log.Fatalf("Invalid type: %T", buf)
	}

	if len(ival) != 6 {
		t.Fatal("No values copied into ival")
	}

	if len(fval) != 6 {
		t.Fatal("No values copied into fval")
	}

	if len(aval) != 6*2 {
		t.Fatal("No values copied into aval")
	}

	if len(tval) != 6 {
		t.Fatal("No values copied into ival")
	}

	for i, v := range vals {
		if v.Fval != fval[i] {
			t.Errorf("Bad value; expected %f, got %f", v.Fval, fval[i])
		}

		if v.Ival != ival[i] {
			t.Errorf("Bad value; expected %d, got %d", v.Ival, ival[i])
		}

		if int32(v.T.Unix()) != tval[i] {
			t.Errorf("Bad value; expected %d, got %d", v.T.Unix(), tval[i])
		}
	}

}
