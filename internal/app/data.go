package app

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"
	"time"

	"bitbucket.org/uwaplmlf2/mlf2sexp"
)

var ByteOrder = binary.BigEndian

func scanDataTo(rec mlf2sexp.Record, v interface{}) error {
	var buf mlf2sexp.Bytes

	err := rec.ScanData(&buf)
	if err != nil {
		return fmt.Errorf("ScanData: %w", err)
	}

	return binary.Read(bytes.NewReader(buf), ByteOrder, v)
}

type Flntu struct {
	T      time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Chlref int16     `var:"chlref" json:"chlref" attrs:"units=counts"`
	Chl    int16     `var:"chl" json:"chl" attrs:"units=counts"`
	Nturef int16     `var:"nturef" json:"nturef" attrs:"units=counts"`
	Ntu    int16     `var:"ntu" json:"ntu" attrs:"units=counts"`
}

func (f *Flntu) UnpackRec(rec mlf2sexp.Record) error {
	var x [4]int16

	err := scanDataTo(rec, &x)
	if err != nil {
		return err
	}

	f.T = rec.Time()
	f.Chlref = x[0]
	f.Chl = x[1]
	f.Nturef = x[2]
	f.Ntu = x[3]

	return nil
}

func (f Flntu) String() string {
	return fmt.Sprintf("%s,%d,%d,%d,%d",
		f.T,
		f.Chlref,
		f.Chl,
		f.Nturef,
		f.Ntu)
}

type Bb2f struct {
	T       time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Blueref int16     `var:"blueref" json:"blueref" attrs:"units=counts"`
	Blue    int16     `var:"blue" json:"blue" attrs:"units=counts"`
	Redref  int16     `var:"redref" json:"refref" attrs:"units=counts"`
	Red     int16     `var:"red" json:"red" attrs:"units=counts"`
	Chl     int16     `var:"chl" json:"chl" attrs:"units=counts"`
}

func (b *Bb2f) UnpackRec(rec mlf2sexp.Record) error {
	var x [5]int16

	err := scanDataTo(rec, &x)
	if err != nil {
		return err
	}

	b.T = rec.Time()
	b.Blueref = x[0]
	b.Blue = x[1]
	b.Redref = x[2]
	b.Red = x[3]
	b.Chl = x[4]

	return nil
}

func (b Bb2f) String() string {
	return fmt.Sprintf("%s,%d,%d,%d,%d,%d",
		b.T,
		b.Blueref,
		b.Blue,
		b.Redref,
		b.Red,
		b.Chl)
}

// Profile data from an SBE41 CTD
type CtdProfile struct {
	T        time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Pr       float32   `var:"pressure" json:"pressure" attrs:"units=decibars"`
	Temp     float32   `var:"temp" json:"temp" attrs:"units=degrees"`
	Salinity float32   `var:"sal" json:"sal" attrs:"units=psu"`
}

func (c CtdProfile) String() string {
	return fmt.Sprintf("%s,%.4f,%.3f,%.3f",
		c.T,
		c.Pr,
		c.Temp,
		c.Salinity)
}

func (c *CtdProfile) UnpackRec(rec mlf2sexp.Record) error {
	f := make([]float32, 3)
	err := scanDataTo(rec, f)
	if err != nil {
		return err
	}

	c.T = rec.Time()
	c.Pr = f[0]
	c.Temp = f[1]
	c.Salinity = f[2]

	return nil
}

// Valeport altimeter data
type Alt struct {
	T     time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Range float32   `var:"range" json:"range" attrs:"units=m"`
	Pr    float32   `var:"pressure" json:"pressure" attrs:"units=decibars"`
}

func (a Alt) String() string {
	return fmt.Sprintf("%s,%.3f,%.3f",
		a.T,
		a.Range,
		a.Pr)
}

func (a *Alt) UnpackRec(rec mlf2sexp.Record) error {
	f := make([]float32, 2)
	err := scanDataTo(rec, f)
	if err != nil {
		return err
	}

	a.T = rec.Time()
	a.Range = f[0]
	a.Pr = f[1]

	return nil
}

type Ctdo struct {
	T        time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Pump     int16     `var:"pump_state" json:"pump_state"`
	Temp     float32   `var:"temp" json:"temp" attrs:"units=degrees"`
	Salinity float32   `var:"sal" json:"sal" attrs:"units=psu"`
	Oxygen   int32     `var:"oxy" json:"oxy"`
}

func (c Ctdo) String() string {
	return fmt.Sprintf("%s,%.3f,%.4f,%d,%d",
		c.T,
		c.Temp,
		c.Salinity,
		c.Oxygen,
		c.Pump)
}

func (c *Ctdo) UnpackRec(rec mlf2sexp.Record) error {
	x := struct {
		T, S float32
		Oxy  int32
	}{}

	c.T = rec.Time()

	var (
		buf   mlf2sexp.Bytes
		state mlf2sexp.Int
		err   error
	)

	if rec.HasList() {
		err = rec.ScanData(&buf, &state)
		if err != nil {
			return err
		}
		c.Pump = int16(state)
	} else {
		err = rec.ScanData(&buf)
		if err != nil {
			return err
		}
		c.Pump = 1
	}

	err = binary.Read(bytes.NewReader(buf), ByteOrder, &x)
	if err != nil {
		return err
	}
	c.Temp = x.T
	c.Salinity = x.S
	c.Oxygen = x.Oxy

	return nil
}

type Optode struct {
	T      time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Oxy    float32   `var:"oxy" json:"oxy"`
	Bphase float32   `var:"bphase" json:"bphase"`
	Rphase float32   `var:"rphase" json:"rphase"`
	Bamp   float32   `var:"bamp" json:"bamp"`
	Ramp   float32   `var:"ramp" json:"ramp"`
	Sat    float32   `var:"sat" json:"sat"`
	Temp   float32   `var:"temp" json:"temp"`
}

func (o Optode) String() string {
	return fmt.Sprintf("%s,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f",
		o.T,
		o.Oxy,
		o.Bphase,
		o.Rphase,
		o.Bamp,
		o.Ramp,
		o.Sat,
		o.Temp)
}

func (o *Optode) fillVals(x float32) {
	o.Oxy = x
	o.Bphase = x
	o.Rphase = x
	o.Bamp = x
	o.Ramp = x
	o.Sat = x
	o.Temp = x
}

func (o *Optode) UnpackRec(rec mlf2sexp.Record) error {
	nan := float32(math.NaN())

	var buf mlf2sexp.Bytes
	err := rec.ScanData(&buf)
	switch n := len(buf); n {
	case 12:
		o.fillVals(nan)
		f := make([]float32, n/4)
		err = binary.Read(bytes.NewReader(buf), ByteOrder, f)
		if err != nil {
			return err
		}
		o.Oxy = f[0]
		o.Temp = f[1]
		o.Bphase = f[2]
	case 16:
		o.fillVals(nan)
		f := make([]float32, n/4)
		err = binary.Read(bytes.NewReader(buf), ByteOrder, f)
		if err != nil {
			return err
		}
		o.Oxy = f[0]
		o.Sat = f[1]
		o.Temp = f[2]
		o.Bphase = f[3]
	case 24:
		o.Oxy = nan
		f := make([]float32, n/4)
		err = binary.Read(bytes.NewReader(buf), ByteOrder, f)
		if err != nil {
			return err
		}
		o.Bphase = f[0]
		o.Rphase = f[1]
		o.Bamp = f[2]
		o.Ramp = f[3]
		o.Sat = f[4]
		o.Temp = f[5]
	default:
		return fmt.Errorf("Unknown Optode record size: %d", n)
	}

	o.T = rec.Time()
	return nil
}

type Sbe63 struct {
	T       time.Time `var:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Oxy     float32   `var:"oxy"`
	Temp    float32   `var:"temp" attrs:"units=degreesC"`
	Phase   float32   `var:"phase" attrs:"units=degrees"`
	Voltage float32   `var:"voltage" attrs:"units=volts"`
}

func (s Sbe63) String() string {
	return fmt.Sprintf("%s,%.2f,%.2f,%.1f,%.3f",
		s.T,
		s.Oxy,
		s.Temp,
		s.Phase,
		s.Voltage)
}

func (s *Sbe63) UnpackRec(rec mlf2sexp.Record) error {
	f := make([]float32, 4)
	err := scanDataTo(rec, f)
	if err != nil {
		return err
	}

	s.T = rec.Time()
	s.Oxy = f[0]
	s.Temp = f[1]
	s.Phase = f[2]
	s.Voltage = f[3]

	return nil
}

type Cstar struct {
	T      time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Output int16     `var:"output" json:"output" attrs:"units=counts"`
}

func (c Cstar) String() string {
	return fmt.Sprintf("%s,%d", c.T, c.Output)
}

func (c *Cstar) UnpackRec(rec mlf2sexp.Record) error {
	var val mlf2sexp.Int
	err := rec.ScanData(&val)
	if err != nil {
		return err
	}

	c.T = rec.Time()
	c.Output = int16(val)
	return nil
}

type Licor struct {
	T        time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Lowgain  int16     `var:"lowgain" json:"lowgain" attrs:"units=mV"`
	Highgain int16     `var:"highgain" json:"highgain" attrs:"units=mV"`
}

func (l Licor) String() string {
	return fmt.Sprintf("%s,%d,%d", l.T, l.Lowgain, l.Highgain)
}

func (l *Licor) UnpackRec(rec mlf2sexp.Record) error {
	var val mlf2sexp.Int
	err := rec.ScanData(&val)
	if err != nil {
		return err
	}

	l.T = rec.Time()
	l.Lowgain = int16(val & 0xffff)
	l.Highgain = int16((val & 0xffff0000) >> 16)

	return nil
}

type Optical struct {
	T   time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Qcp int16     `var:"qcp" json:"qcp" attrs:"units=mV"`
	Mcp int16     `var:"mcp" json:"mcp" attrs:"units=mV"`
}

func (o Optical) String() string {
	return fmt.Sprintf("%s,%d,%d", o.T, o.Qcp, o.Mcp)
}

func (o *Optical) UnpackRec(rec mlf2sexp.Record) error {
	var val mlf2sexp.Int
	err := rec.ScanData(&val)
	if err != nil {
		return err
	}

	o.T = rec.Time()
	o.Qcp = int16(val & 0xffff)
	o.Mcp = int16((val & 0xffff0000) >> 16)

	return nil
}

type Gtd struct {
	T        time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Pr       float32   `var:"pressure" json:"pressure"`
	Temp     float32   `var:"temperature" json:"temperature"`
	IntTime  int16     `var:"pres_res" json:"pres_res"`
	TempTime int16     `var:"temp_res" json:"temp_res"`
	Mode     int16     `var:"pump_mode" json:"pump_mode"`
	OnTime   int16     `var:"pump_on" json:"pump_on"`
	OffTime  int16     `var:"pump_off" json:"pump_off"`
	Cycle    int16     `var:"pump_cycle" json:"pump_cycle"`
	Delay    int16     `var:"pump_delay" json:"pump_delay"`
	BbDelay  int16     `var:"bb_delay" json:"bb_delay"`
	BbWarmup int16     `var:"bb_warmup" json:"bb_warmup"`
	Flushed  int16     `var:"flushed" json:"flushed"`
}

type Fpr struct {
	T   time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Pr0 float32   `var:"pressure0" json:"pr0" attrs:"units=decibars"`
	Pr1 float32   `var:"pressure1" json:"pr1" attrs:"units=decibars"`
	Pr2 float32   `var:"pressure2" json:"pr2" attrs:"units=decibars"`
}

func (f Fpr) String() string {
	return fmt.Sprintf("%s,%.3f,%.3f,%.3f",
		f.T, f.Pr0, f.Pr1, f.Pr2)
}

func NewFprs(rec mlf2sexp.Record) ([]Fpr, error) {
	npr := rec.Len()
	if npr == 0 {
		return nil, fmt.Errorf("Invalid record format: %#v", rec)
	}

	buf := make([]mlf2sexp.Bytes, npr)
	err := rec.ScanData(&buf)
	if err != nil {
		return nil, err
	}

	nvals := len(buf[0]) / 4
	vals := make([][]float32, npr)
	for i := 0; i < npr; i++ {
		vals[i] = make([]float32, nvals)
		err = binary.Read(bytes.NewReader(buf[i]), ByteOrder, vals[i])
		if err != nil {
			return nil, err
		}
	}

	data := make([]Fpr, nvals)
	t0 := rec.Time()
	for i := 0; i < nvals; i++ {
		data[i].T = t0.Add(time.Second * time.Duration(i))
		data[i].Pr0 = vals[0][i]
		if npr > 1 {
			data[i].Pr1 = vals[1][i]
		}
		if npr > 2 {
			data[i].Pr2 = vals[2][i]
		}
	}

	return data, nil
}

type Palargo struct {
	T   time.Time  `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Spl [8]float32 `var:"spl" dims:"frequency" json:"spl"`
}

func (p Palargo) String() string {
	return fmt.Sprintf("%s,%.1f", p.T, p.Spl)
}

func (p *Palargo) UnpackRec(rec mlf2sexp.Record) error {
	var scaled [8]int16
	p.T = rec.Time()
	err := scanDataTo(rec, &scaled)
	if err != nil {
		return err
	}
	for i, x := range scaled {
		p.Spl[i] = float32(x) * 0.1
	}

	return nil
}

// Par contains a data record from a PAR sensor
type Par struct {
	T    time.Time `var:"time" json:"time" attrs:"units=seconds since 1/1/1970 UTC"`
	Irr  float32   `var:"irr" json:"irr" attrs:"units=uE/cm^2/s"`
	Temp float32   `var:"temp" json:"temp" attrs:"units=degC"`
	V    float32   `var:"v" json:"v" attrs:"units=volts"`
}

func (p Par) String() string {
	return fmt.Sprintf("%s,%.2f,%.2f,%.3f", p.T, p.Irr, p.Temp, p.V)
}

func (p *Par) UnpackRec(rec mlf2sexp.Record) error {
	f := make([]float32, 3)
	err := scanDataTo(rec, f)
	if err != nil {
		return err
	}
	p.T = rec.Time()
	p.Irr = f[0]
	p.Temp = f[1]
	p.V = f[2]

	return nil
}
