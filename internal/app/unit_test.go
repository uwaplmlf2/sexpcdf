package app

import (
	"fmt"
	"log"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/uwaplmlf2/mlf2sexp"
)

func TestStoreId(t *testing.T) {
	table := []struct {
		name string
		id   StoreId
	}{
		{name: "data0001.sx", id: StoreId{dir: "data0001", suffix: "0001"}},
		{name: "ql-20220711_230203.sx",
			id: StoreId{dir: "ql-20220711_230203", suffix: "20220711_230203"}},
	}

	for _, e := range table {
		id, err := NewStoreId(e.name)
		if err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}
		if !reflect.DeepEqual(id, e.id) {
			t.Errorf("Bad value; expected %#v, got %#v", e.id, id)
		}
	}

}

func ExampleCtdo() {
	input := "(ctdo #5abdf6fe# (|QRvvNUHcmwkAAAAA| #1#))\n"
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	ctdo := &Ctdo{}
	for s.Scan() {
		rec := s.Record()
		ctdo.UnpackRec(rec)
		fmt.Printf("%s\n", ctdo)
	}
	// Output:
	// 2018-03-30 08:36:14 +0000 UTC,9.746,27.5757,0,1
}

func ExampleAlt() {
	input := "(alt #62bbdee2# |AAAAAD3fO2Q=|)\n"
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	alt := &Alt{}
	for s.Scan() {
		rec := s.Record()
		alt.UnpackRec(rec)
		fmt.Printf("%s\n", alt)
	}
	// Output:
	// 2022-06-29 05:10:58 +0000 UTC,0.000,0.109
}

func ExampleOptode() {
	input := `(optode #5dd3f318# |QhEBBkCAGJNEasAARCjzM0K1PXFBol41|)`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	o := Optode{}
	for s.Scan() {
		rec := s.Record()
		o.UnpackRec(rec)
		fmt.Printf("%s\n", o)
	}
	// Output:
	// 2019-11-19 13:50:16 +0000 UTC,NaN,36.25,4.00,939.00,675.80,90.62,20.30
}

func ExampleFpr() {
	input := `(fpr #55f0e2dc# (|Pk1PjT5YMuw+T1QhPjRMIj5Z1qQ+QqH+PjB4yT5QVms+R8L2Pljp4T4E604+U0fDPh/oij44CfU+QVRePlqjHz5HrXA+VKrpPi9rvD5T8/U+SlNyPjmCoA==| |Px7uXz8fnsw/FwQkPx1tcT8bvGU/FkkGPxhU9T8dJ/E/His8Px5TVT8Xny8/HMe1PxkoIj8XBCQ/Gx6uPyE6dj8cXMg/G5b5PxouGT8dDTY/GaBsPxfXUQ==| |Px77fj8df+Y/FcBhPx3tgD8Xdsk/FfiEPxmgIz8iODA/Iss3PyF/vD8dyr8/IXe3Px3oJz8bG7w/ITzoPyYCkD8evgI/IMn1Px7LYD8hV6M/HYU+PxeW3Q==| ))`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	for s.Scan() {
		rec := s.Record()
		data, _ := NewFprs(rec)
		n := len(data)
		fmt.Printf("%s\n", data[0])
		fmt.Printf("%s\n", data[n-1])
	}
	// Output:
	// 2015-09-10 01:54:36 +0000 UTC,0.200,0.621,0.621
	// 2015-09-10 01:54:57 +0000 UTC,0.181,0.593,0.592
}

func ExampleFprSingle() {
	input := `(fpr #55f0e2dc# (|Pk1PjT5YMuw+T1QhPjRMIj5Z1qQ+QqH+PjB4yT5QVms+R8L2Pljp4T4E604+U0fDPh/oij44CfU+QVRePlqjHz5HrXA+VKrpPi9rvD5T8/U+SlNyPjmCoA==|))`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	for s.Scan() {
		rec := s.Record()
		data, _ := NewFprs(rec)
		n := len(data)
		fmt.Printf("%s\n", data[0])
		fmt.Printf("%s\n", data[n-1])
	}
	// Output:
	// 2015-09-10 01:54:36 +0000 UTC,0.200,0.000,0.000
	// 2015-09-10 01:54:57 +0000 UTC,0.181,0.000,0.000
}

func ExamplePalargo() {
	input := `(pal #576eb362# |AaEBWgFTAc4BdgFqAWYBag==|)
(pal #577109ec# |AW0BWQFbAb8BfwFxAW8BaQ==|)
`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	p := Palargo{}
	for s.Scan() {
		rec := s.Record()
		p.UnpackRec(rec)
		fmt.Printf("%s\n", p)
	}
	// Output:
	// 2016-06-25 16:37:54 +0000 UTC,[41.7 34.6 33.9 46.2 37.4 36.2 35.8 36.2]
	// 2016-06-27 11:11:40 +0000 UTC,[36.5 34.5 34.7 44.7 38.3 36.9 36.7 36.1]
}

func ExampleCtdProfile() {
	input := `(ctdprof #62ab9995# |voAAAEGnckcAAAAA|)
(ctdprof #62ab9996# |voAAAEGncXY8LAgx|)
`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	c := CtdProfile{}
	for s.Scan() {
		rec := s.Record()
		if err := c.UnpackRec(rec); err != nil {
			log.Printf("error: %v", err)
		}

		fmt.Printf("%s\n", c)
	}
	// Output:
	// 2022-06-16 20:59:01 +0000 UTC,-0.2500,20.931,0.000
	// 2022-06-16 20:59:02 +0000 UTC,-0.2500,20.930,0.010
}

func ExamplePar() {
	input := `(qcp #55f20c84# |O6PXCkHguFJBI2BC|)`
	s := mlf2sexp.NewScanner(strings.NewReader(input))
	x := Par{}
	for s.Scan() {
		rec := s.Record()
		if err := x.UnpackRec(rec); err != nil {
			log.Printf("error: %v", err)
		}

		fmt.Printf("%s\n", x)
	}
	// Output:
	// 2015-09-10 23:04:36 +0000 UTC,0.00,28.09,10.211
}
