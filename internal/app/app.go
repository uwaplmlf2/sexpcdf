// Package app converts an MLF2 s-expression data files to a collection of
// one or more netCDF data files.
package app

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"bitbucket.org/uwaplmlf2/mlf2sexp"
	"bitbucket.org/uwaplmlf2/sexpcdf/internal/ncfile"
)

// Pair represents a key=value pair
type Pair struct {
	Name, Value string
}

func (p Pair) String() string {
	return fmt.Sprintf("%s=%s", p.Name, p.Value)
}

// Pairs is a collection of Pair values that supports the flag.Value
// interface allowing them to be specified on the command line as --flag
// name=value.
type Pairs []Pair

func (pp *Pairs) String() string {
	return fmt.Sprint(*pp)
}

func (pp *Pairs) Set(val string) error {
	parts := strings.Split(val, "=")
	*pp = append(*pp, Pair{Name: parts[0], Value: parts[1]})
	return nil
}

func extractData(rdr io.Reader, ds *DataStore, att Pairs) error {
	s := mlf2sexp.NewScanner(rdr)

	attrs := ncfile.Attributes{}
	attrs[""] = make(map[string]interface{})
	for _, p := range att {
		attrs[""][p.Name] = p.Value
	}

	for s.Scan() {
		rec := s.Record()
		switch rec.Sensor {
		case "ctdo":
			ds.addCtdo(rec, attrs)
		case "flntu":
			ds.addFlntu(rec, attrs)
		case "sbe63":
			ds.addSbe63(rec, attrs)
		case "fpr":
			ds.addFpr(rec, attrs)
		case "cstar":
			ds.addCstar(rec, attrs)
		case "optode":
			ds.addOptode(rec, attrs)
		case "suna":
			ds.addSuna(rec, attrs)
		case "ctdprof", "ctdprof-0", "ctdprof-1":
			ds.addCtdProfile(rec, attrs)
		case "alt":
			ds.addAlt(rec, attrs)
		case "qcp", "mcp":
			ds.addPar(rec, attrs)
		case "err":
		default:
			log.Printf("Unsupported record type: %q", rec.Sensor)
		}
	}

	return s.Err()
}

func Run(r io.Reader, id StoreId, att Pairs, output io.Writer) error {
	if err := os.MkdirAll(id.String(), 0755); err != nil {
		return fmt.Errorf("mkdir %s: %w", id.String(), err)
	}

	ds := NewDataStore(id)
	err := extractData(r, ds, att)
	if err != nil {
		return err
	}

	return ds.Store(output)
}
