package app

import (
	"archive/zip"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"bitbucket.org/uwaplmlf2/mlf2sexp"
	"bitbucket.org/uwaplmlf2/sexpcdf/internal/ncfile"
)

type RecordStore interface {
	WriteRecords(v interface{}) error
	Close() error
}

type StoreId struct {
	dir, suffix string
}

func NewStoreId(filename string) (StoreId, error) {
	var id StoreId

	if strings.HasPrefix(filename, "ql-") {
		if idx := strings.LastIndex(filename, "."); idx >= 0 {
			filename = filename[:idx]
		}
		id.dir = filename
		id.suffix = filename[3:]
	} else {
		re, err := regexp.Compile(`(?P<prefix>[a-zA-Z]+)(?P<suffix>\d+)\..*`)
		if err != nil {
			return id, fmt.Errorf("regexp compile: %w", err)
		}

		if !re.MatchString(filename) {
			return id, fmt.Errorf("Invalid filename: %q", filename)
		}
		matches := re.FindStringSubmatch(filename)
		id.dir = strings.ToLower(matches[re.SubexpIndex("prefix")] + matches[re.SubexpIndex("suffix")])
		id.suffix = matches[re.SubexpIndex("suffix")]
	}

	return id, nil
}

func (id StoreId) String() string {
	return id.dir
}

type DataStore struct {
	files       map[string]RecordStore
	recs        map[string]interface{}
	dir, suffix string
}

func NewDataStore(id StoreId) *DataStore {
	if id.dir == "" {
		id.dir = "."
	}

	return &DataStore{
		files:  make(map[string]RecordStore),
		recs:   make(map[string]interface{}),
		dir:    id.dir,
		suffix: id.suffix,
	}
}

func openDir(dir string) (chan os.FileInfo, error) {
	d, err := os.Open(dir)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.FileInfo, 1)
	go func() {
		defer close(ch)
		defer d.Close()

		for {
			fi, err := d.Readdir(1)
			if err != nil {
				return
			}
			ch <- fi[0]
		}
	}()

	return ch, nil
}

// Store writes the data store records to their respective files. If wtr is
// non-nil, the files are ZIP compressed and written to w then the files are
// removed from the filesystem.
func (d *DataStore) Store(wtr io.Writer) error {
	for k, v := range d.files {
		defer v.Close()
		v.WriteRecords(d.recs[k])
	}

	if wtr == nil {
		return nil
	}

	w := zip.NewWriter(wtr)

	ch, err := openDir(d.dir)
	if err != nil {
		return err
	}

	for fi := range ch {
		path := filepath.Join(d.dir, fi.Name())
		body, err := os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("read %s: %w", path, err)
		}
		f, err := w.Create(fi.Name())
		if err != nil {
			return fmt.Errorf("create %s: %w", fi.Name(), err)
		}
		_, err = f.Write(body)
		if err != nil {
			return fmt.Errorf("write %s: %w", fi.Name(), err)
		}
	}

	err = w.Close()
	if err != nil {
		return err
	}

	if d.dir == "." {
		return nil
	}

	return os.RemoveAll(d.dir)
}

func (d *DataStore) addCtdo(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Ctdo{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Ctdo{}
	}

	objs := d.recs[rec.Sensor].([]Ctdo)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addAlt(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Alt{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Alt{}
	}

	objs := d.recs[rec.Sensor].([]Alt)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addFlntu(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Flntu{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Flntu{}
	}

	objs := d.recs[rec.Sensor].([]Flntu)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addSbe63(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Sbe63{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Sbe63{}
	}

	objs := d.recs[rec.Sensor].([]Sbe63)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addCtdProfile(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := CtdProfile{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []CtdProfile{}
	}
	objs := d.recs[rec.Sensor].([]CtdProfile)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addFpr(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj, err := NewFprs(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj[0], attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Fpr{}
	}

	objs := d.recs[rec.Sensor].([]Fpr)
	objs = append(objs, obj...)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addCstar(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Cstar{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Cstar{}
	}

	objs := d.recs[rec.Sensor].([]Cstar)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addOptode(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Optode{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Optode{}
	}

	objs := d.recs[rec.Sensor].([]Optode)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

func (d *DataStore) addPar(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	obj := Par{}
	err := obj.UnpackRec(rec)
	if err != nil {
		return err
	}

	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.nc", rec.Sensor, d.suffix))
		nc, err := ncfile.NewFile(path, obj, attrs)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = nc
		d.recs[rec.Sensor] = []Par{}
	}

	objs := d.recs[rec.Sensor].([]Par)
	objs = append(objs, obj)
	d.recs[rec.Sensor] = objs

	return nil
}

// Wrap an os.File so it supports the RecordStore interface
type File struct {
	*os.File
}

func (f *File) WriteRecords(v interface{}) error {
	return nil
}

func (f *File) Close() error {
	return f.File.Close()
}

// SUNA data is not stored as netCDF
func (d *DataStore) addSuna(rec mlf2sexp.Record, attrs ncfile.Attributes) error {
	var (
		b    []mlf2sexp.Bytes
		mask mlf2sexp.Int
		err  error
	)

	switch n := rec.Len(); n {
	case 2:
		b = make([]mlf2sexp.Bytes, n-1)
		err = rec.ScanData(&mask, &b[0])
	case 3:
		b = make([]mlf2sexp.Bytes, n-1)
		err = rec.ScanData(&mask, &b[0], &b[1])
	default:
		return fmt.Errorf("Invalid data section length: %d", n)
	}

	if err != nil {
		return err
	}

	t := rec.Time()
	if _, found := d.files[rec.Sensor]; !found {
		path := filepath.Join(d.dir, fmt.Sprintf("%s_%s.dat", rec.Sensor, d.suffix))
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		log.Printf("Creating %s ...", path)
		d.files[rec.Sensor] = &File{File: file}
		d.recs[rec.Sensor] = nil
	}

	f := d.files[rec.Sensor].(*File)

	secs := int32(t.Unix())
	for _, buf := range b {
		binary.Write(f.File, binary.BigEndian, secs)
		_, err = f.File.Write(buf)
		if err != nil {
			return err
		}
	}

	return nil
}
